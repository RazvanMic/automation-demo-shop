
import com.codeborne.selenide.Configuration;

import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.selenide.AllureSelenide;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.CartPage;

import static com.codeborne.selenide.Selenide.open;

import static org.testng.Assert.*;
import static pages.Constants.CART_PAGE;

public class DemoShopCartTest {
CartPage cartPage;

   @BeforeTest
    public void before(){
       SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(true));
       Configuration.baseUrl =CART_PAGE;
       open(CART_PAGE);
       cartPage = new CartPage();


    }
        @Test
        @Severity(SeverityLevel.CRITICAL)
    public void verify_payment_information_method(){
            assertEquals(cartPage.titleInformation(),"Your cart", "The Cart message was displayed.");
       }

       @Test
    public void page_object_message(){
           assertEquals(cartPage.objectTitle(), "How about adding some products in your cart?",
                   "The message from Page Objects was displayed.");
       }

}
