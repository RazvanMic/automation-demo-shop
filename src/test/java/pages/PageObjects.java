package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class PageObjects {
    private final  SelenideElement products = $(".subheader-container");


    public void searchingWitheElementAwesome(){
        SelenideElement searchBar = $("#input-search");
        SelenideElement searchButton = $(".btn-light ");
        searchBar.setValue("awesome");
        searchButton.click();
    }

    public String textInformationProduct () {
        return  products.getText();
    }
}
