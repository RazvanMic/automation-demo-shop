package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Footer {
    private final SelenideElement helpElement = $("[data-icon='question']");
    private final SelenideElement undoElement = $(".nav-item .fa-undo");

    public boolean questionMark(){
        return helpElement.exists();
    }
    public void helpWindow(){
        helpElement.click();
    }
    public boolean restartingThePage(){
        return undoElement.exists();
    }

}
