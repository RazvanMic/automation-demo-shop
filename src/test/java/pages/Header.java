package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Header {


    private final SelenideElement greetings = $(".navbar-text > span");
    private final SelenideElement logo = $(".navbar-brand .fa-shopping-bag");
    private final SelenideElement wishListIcon = $(".navbar-nav [data-icon=heart]");
    private final SelenideElement cartIcon = $(".fa-shopping-cart");
    private final SelenideElement loginElement = $(".fa-sign-in-alt");
    private final SelenideElement headerElement = $("#responsive-navbar-nav");
    private final SelenideElement shoppingCartBadge = $("[data-icon=shopping-cart]~.shopping_cart_badge");
    private final SelenideElement favoritesIcon = $("[data-icon=heart]~.shopping_cart_badge");


    public void loginWithSpecificUser() {
        SelenideElement loginButton = $(".fa-sign-in-alt");
        SelenideElement username = $("#user-name");
        SelenideElement password = $("#password");
        loginButton.click();
        username.setValue("dino");
        password.setValue("choochoo");
        SelenideElement LoginIntoAccount = $(".btn-primary");
        LoginIntoAccount.click();
    }
    public String favIcon(){
        return favoritesIcon.getText();
    }

    public String greetingsMessage() {

        return greetings.getText();
    }

    public String logoRedirectURL() {
        return logo.parent().getAttribute("href");
    }

    public String wishlistRedirectURL() {
        return wishListIcon.parent().getAttribute("href");
    }

    public String cartRedirectURL() {
        return cartIcon.parent().getAttribute("href");
    }

    public String productsInCartBadge() {
        return shoppingCartBadge.getText();
    }


    public boolean isFavoritesIconDisplayed() {
        return wishListIcon.exists() && wishListIcon.isDisplayed();
    }

    public boolean isGreetingsMessageDisplayed() {

        return greetings.exists() && greetings.isDisplayed();
    }

    public boolean isLogoDisplayed() {
        return logo.exists() && logo.isDisplayed();
    }

    public boolean isCartIconDisplayed() {
        return cartIcon.exists() && cartIcon.isDisplayed();
    }
    public boolean loginElementDisplayed(){
        return loginElement.exists() && greetings.isDisplayed();
    }
    public boolean HeaderBar(){
        return headerElement.exists() && greetings.isDisplayed();
    }
    public void basketIcon(){
        SelenideElement basketIcon = $("[data-icon=shopping-cart]~.shopping_cart_badge");
        basketIcon.click();
    }
    public void addBasketElement(){
        SelenideElement basketIcon = $("[data-icon=shopping-cart]~.shopping_cart_badge");
        basketIcon.click();
    }

}


