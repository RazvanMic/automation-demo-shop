package pages;



public class Constants {
    public static final String HELLO_GUEST_GREETINGS_MSG = "Hello guest!";
    public static final String HOME_PAGE = "https://fasttrackit-test.netlify.app/";
    public static final String  CART_PAGE = "https://fasttrackit-test.netlify.app/#/cart";
    public static final String AWESOME_GRANITE_CHIPS_PRODUCT_ID= "1";
    public static final String AWESOME_METAL_CHAIR_PRODUCT_ID= "3";
    public static final String PRACTICAL_WOODEN_BACON_PRODUCT_ID= "9";

}
