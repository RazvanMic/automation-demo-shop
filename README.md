# Test Demo Shop automation Framework
## Name
Miclea Razvan

Automation-Demo-Shop

## Description
Automation is an automation project developed in Java language to implement a suit of tests with scope to see functionality of application:
Login, Register, MyAccount, Products, Wishlist, MyCart, etc.


-The Tech used to implement thous tests was:

-Browser: Google Chrome 100.0.4896.75

-URL: https://fasttrackit-test.netlify.app/#/

-Langue used: Java

**IDE**-An integrated development environment (IDE) is a software application that provides comprehensive facilities to computer
programmers for software development. An IDE normally consists of at least a source code editor, build automation tools
and a debugger. Some IDEs, such as NetBeans and Eclipse, contain the necessary compiler, interpreter, or both; others,
such as SharpDevelop and Lazarus, do not.

**TestNG** is a testing framework for the Java programming language created by Cédric Beust and inspired by JUnit and NUnit.
The design goal of TestNG is to cover a wider range of test categories: unit, functional, end-to-end, integration, etc..

**Selenide** is a framework for test automation powered by Selenium WebDriver that brings the following advantages:
Concise fluent API for tests Ajax support for stable tests powerful selectors, simple configuration.
You don't need to think how to shutdown browser, handle timeouts and StaleElement Exceptions or search for relevant log lines, debugging your tests.

**Maven** is a build automation tool used primarily for Java projects. Maven can also be used to build and manage projects written in C#, Ruby, Scala, and other languages.
The Maven project is hosted by the Apache Software Foundation, where it was formerly part of the Jakarta Project.

**Allure Framework** is a flexible lightweight multi-language test report tool that not only shows a very concise representation of what have been tested in a neat web report form,
but allows everyone participating in the development process to extract maximum of useful information from everyday execution of tests.




### Tech stock used:

```
-Google Chrome
-Discord
-Gitlab
-Java
-Allure
-Maven
-Selenide Framework
-PageObjectModels
```

### Page Objects
```
-HomePage
-Header
-Footer
-Body/Products
```


## Add your files

- [ ] [Automation Demo Shop](https://gitlab.com/RazvanMic/automation-demo-shop.git) 

```
cd existing_repo
git remote add origin https://gitlab.com/RazvanMic/automation-demo-shop.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [GitLab-link](https://gitlab.com/RazvanMic/automation-demo-shop/-/settings/integrations)
- [ ] [Discord-Invite-Link](https://discord.gg/SYe63Uvq)



