import com.codeborne.selenide.Configuration;

import org.testng.annotations.*;
import pages.*;

import static com.codeborne.selenide.Selenide.*;
import static org.testng.Assert.*;
import static pages.Constants.*;


public class DemoShopHomeTest {

    Header header;
    Footer footer;
    PageObjects pageElements;
    CartPage cartPage;



    @BeforeMethod
    public void openPage() {
        Configuration.baseUrl = HOME_PAGE;
        open(HOME_PAGE);
        header = new Header();
        footer = new Footer();
        pageElements = new PageObjects();
        cartPage = new CartPage();
    }

    @AfterMethod
    public void driverCleanUp() {
        webdriver().driver().close();
    }

    @Test
    public void verify_greeting_message_is_welcome() {
        assertTrue(header.isGreetingsMessageDisplayed());
        assertEquals(header.greetingsMessage(), HELLO_GUEST_GREETINGS_MSG, "welcome message is not displayed.");
    }

    @Test
    public void verify_homePage_logo() {
        assertTrue(header.isLogoDisplayed(), "Logo exists in navigation bar.");
        assertEquals(header.logoRedirectURL(), HOME_PAGE, "Clicking the logo, redirect to home page.");
    }

    @Test
    public void verify_wishlist_icon() {
        assertTrue(header.isFavoritesIconDisplayed(), "Wishlist icon is displayed.");
        assertEquals(header.wishlistRedirectURL(), HOME_PAGE + "#/wishlist", "Page redirected to Wishlist Page");
    }

    @Test
    public void verify_cart_button() {
        assertTrue(header.isCartIconDisplayed(), "Cart icon is displayed");
        assertEquals(header.cartRedirectURL(), HOME_PAGE + "#/cart", "Page redirected to Cart Page.");
    }

    @Test
    public void verify_login_button() {
        assertTrue(header.loginElementDisplayed(), "Login button is displayed on header.");
    }

    @Test
    public void verify_header_is_visible() {
        assertTrue(header.HeaderBar(), "Header is visible on page.");
    }

    @Test
    public void Perform_login_with_a_specific_account() {
        header.loginWithSpecificUser();
        assertTrue(header.isGreetingsMessageDisplayed());
        assertEquals(header.greetingsMessage(), "Hi dino!", "Welcome message is not displayed.");
        sleep(2000);
    }

    @Test
    public void verify_AwesomeGraniteChipsURL() {
        Product products = new Product(AWESOME_GRANITE_CHIPS_PRODUCT_ID);
        assertTrue(products.isDisplayed(), "Product 1 is displayed on page.");
        assertEquals(products.url(), HOME_PAGE + "#/product/1", "Product 1 page is open.");
    }

    @Test

    public void cart_icon_update_upon_adding_a_product_to_cart() {
        Product products = new Product(AWESOME_GRANITE_CHIPS_PRODUCT_ID);
        products.productAddToBasket();
        assertEquals(header.productsInCartBadge(), "1",
                "After adding a product to basket, cart icon is updated");
    }

    @Test
    public void favorites_icon_update_upon_adding_a_product_to_favorites() {
        Product product = new Product(AWESOME_METAL_CHAIR_PRODUCT_ID);
        product.addToWishlist();
        assertFalse(product.IsInWishlist(),
                "Adding a product to favorites");
        assertEquals(header.favIcon(), "1",
                "After adding a product to favorites the ,favorite icon updates");
    }

    @Test
    public void verify_helpButton_is_opening_a_newWindow() {
        assertTrue(footer.questionMark(), "Help button is displayed");
        footer.helpWindow();
    }

    @Test
    public void search_button_is_looking_for_awesome_items() {
        pageElements.searchingWitheElementAwesome();
    }

    @Test
    public void products_on_page_objects()  {
        assertEquals(pageElements.textInformationProduct(),
                "Products", "The products text is displayed.");
    }

    @Test
    public void checking_refresh_element() {
        assertTrue(footer.restartingThePage(), "The refreshing button is restarting the page.");
    }


    @Test
    public void verify_checkout_method_after_selecting_a_product() {
        Product products = new Product(AWESOME_GRANITE_CHIPS_PRODUCT_ID);
        products.productAddToBasket();
        assertTrue(products.isDisplayed(), "Product 1 is displayed on page.");
        header.basketIcon();
        cartPage.checkout();
    }

    @Test
    public void gorgeous_soft_pizza_text() {
        Product products = new Product(PRACTICAL_WOODEN_BACON_PRODUCT_ID);
        assertEquals(products.clickOnProductSoftPizza(), "Gorgeous Soft Pizza",
                "The text is displayed.");
        products.gorgeousSoftPizza();
    }

    @Test
    public void verify_continue_shopping_after_selecting_a_product() {
        Product products = new Product(AWESOME_GRANITE_CHIPS_PRODUCT_ID);
        products.productAddToBasket();
        assertTrue(products.isDisplayed(), "Product 1 is displayed on page.");
        header.addBasketElement();
        cartPage.dangerButtonToContinueShopping();

    }


}

