package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CartPage {
    private final SelenideElement textInformation = $(".row .text-muted");
    private final SelenideElement pageObject = $(".text-center ");

    public String objectTitle(){
        return pageObject.getText();
    }

    public String titleInformation(){
        return textInformation.getText();
    }
    public void checkout(){
        SelenideElement checkout = $(".btn-success");
        checkout.click();
    }
    public void dangerButtonToContinueShopping(){
        SelenideElement continueShopping = $("a.btn.btn-danger");
        continueShopping.click();
    }
    public void aaa(){

    }
}
