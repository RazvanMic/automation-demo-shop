package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static java.lang.String.*;

public class Product{
    private final SelenideElement title;
    private final SelenideElement mainProductCard;
    private final SelenideElement addToBasketIcon;
    private final SelenideElement productPizza;

    public Product(String productID) {
        this.title = $(format("[href='#/product/%s']", productID));
        this.mainProductCard = title.parent().parent();
        this.addToBasketIcon = mainProductCard.$("[data-icon=cart-plus]");
        this.productPizza =title;
    }
    public String clickOnProductSoftPizza(){
        return title.getText();

    }


    public void addToWishlist(){
        SelenideElement mainProductCart = title.parent().parent();
        SelenideElement favoritesButton = mainProductCart.$("[data-icon*=heart]");
        favoritesButton.click();
    }


    public boolean isDisplayed(){
            return title.exists() && title.isDisplayed();
    }
    public String url(){
        return title.getAttribute("href");
    }
    public void productAddToBasket(){
        addToBasketIcon.click();
    }
     public boolean IsInWishlist(){
        return addToBasketIcon.getAttribute("data-icon") ==("heart-broken");
     }
     public void gorgeousSoftPizza(){
         title.click();
     }


}
